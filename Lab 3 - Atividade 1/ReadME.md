# Jogo de Cartas em Java

Na atividade simula um jogo de cartas entre o computador e o jogador. O jogo utiliza um baralho padrão de 52 cartas com quatro naipes (Paus, Ouro, Copas, Espadas) e valores de 2 a 10, além de "J", "Q", "K" e "A" representando Valete, Rainha, Rei e Ás, respectivamente.

## Estrutura do Programa

Você vai ter os seguintes arquivos:

- `Carta.java`: Representa uma única carta de baralho.
- `Deck.java`: Representa um baralho de cartas.
- `Suit.java`: Enumera os quatro naipes do baralho.
- `Main.java`: Ponto de entrada do programa.

## Funcionamento

1. O programa cria um novo baralho e o embaralha.
2. O computador e o jogador retiram uma carta cada do baralho.
3. O programa exibe as cartas sorteadas e determina o vencedor com base nos valores das cartas.
4. O vencedor (ou empate) é exibido no console.

## Diagrama de classes
![diagrama de classes](Assets/classes.png)

## Fluxograma
![fluxograma](Assets/fluxograma.png)

## Execução
![executar](Assets/execucao.png)

***
<div class="profile-link">
  <a href="https://gitlab.com/hey_luannlucas" style="display: flex; align-items: center; text-decoration: none;">
    <img src="Assets/icons8-gitlab.svg" alt="GitLab" width="60" height="60" style="margin-right: 10px; border-radius: 50%;">
    <span class="name" style="font-size: 24px; font-weight: bold; color: #0000f;">Luann Lucas</span>
  </a>
</div>

