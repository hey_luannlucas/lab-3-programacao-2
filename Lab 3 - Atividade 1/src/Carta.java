public class Carta {
    private final Suit suit;
    private final String valor;
    public Carta(Suit suit, String valor) {
        this.suit = suit;
        this.valor = valor;
    }
    public String getValor(){
        return valor;
    }
    @Override
    public String toString(){
        return suit.getSimbolo() + valor;
    }
}