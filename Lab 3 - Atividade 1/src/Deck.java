import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
    private final List<Carta> cartas;

    public Deck() {
        cartas = new ArrayList<>();

        // Preenche o baralho com todas as combinações possíveis de naipes e valores
        for (Suit suit : Suit.values()) {
            for (int valor = 2; valor <= 10; valor++) {
                cartas.add(new Carta(suit, String.valueOf(valor)));
            }
            cartas.add(new Carta(suit, "J"));
            cartas.add(new Carta(suit, "Q"));
            cartas.add(new Carta(suit, "K"));
            cartas.add(new Carta(suit, "A"));
        }

        // Embaralha o baralho
        Collections.shuffle(cartas);
    }
    public Carta pegarCarta() {
        if (!cartas.isEmpty()) {
            return cartas.remove(0);
        }
        return null;
    }
}