public class Main {
    public static void main(String[] args) {
        Deck deck = new Deck();

        Carta cartaComputador = deck.pegarCarta();
        Carta cartaJogador = deck.pegarCarta();

        if (cartaComputador != null && cartaJogador != null) {
            System.out.println("Computador: " + cartaComputador);
            System.out.println("Jogador: " + cartaJogador);

            int valorComputador = obterValorParaCarta(cartaComputador);
            int valorJogador = obterValorParaCarta(cartaJogador);

            if (valorComputador > valorJogador) {
                System.out.println("PERDEU!");
            } else if (valorComputador < valorJogador) {
                System.out.println("GANHOU!");
            } else {
                System.out.println("Empate");
            }
        } else {
            System.out.println("O baralho está vazio.");
        }
    }

    // Retorna o valor inteiro associado a uma carta
    private static int obterValorParaCarta(Carta carta) {
        String valor = carta.getValor();
        if ("J".equals(valor) || "Q".equals(valor) || "K".equals(valor)) {
            return 11; // Todas as cartas J, Q, K têm o valor 11 para fins de comparação
        } else if ("A".equals(valor)) {
            return 1; // A carta A tem o valor 1 ou 11, optamos por 1 para fins de comparação
        }
        return Integer.parseInt(valor);
    }
}