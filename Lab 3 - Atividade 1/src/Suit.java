
    public enum Suit {
        PAUS("P"),
        OURO("O"),
        COPAS("C"),
        ESPADAS("E");

        private final String simbolo;

        Suit(String simbolo) {
            this.simbolo = simbolo;
        }

        public String getSimbolo(){
            return simbolo;
        }
    }

