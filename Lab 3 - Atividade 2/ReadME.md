# Gerenciamento de Cartões e Contas Bancárias

Essa atividade foi feita para gerenciar cartões de crédito, débito e pré-pagos, bem como suas contas associadas. O sistema permite que os usuários efetuem saques de acordo com as restrições de cada tipo de cartão.

## Diagrama

![diagrama de classes](Assets/diagrama%20de%20classes.png)

## Classes

1. `Account` (Conta): Classe abstrata que representa uma conta. Ela possui um atributo de saldo (`balance`) e métodos para obter e definir o saldo.

2. `CreditCard` (Cartão de Crédito): Classe abstrata que representa um cartão de crédito associado a uma conta. Ela estende a classe `Card`.

3. `DebitCard` (Cartão de Débito): Classe abstrata que representa um cartão de débito associado a uma conta corrente. Ela estende a classe `Card`.

4. `PrepaidCard` (Cartão Pré-pago): Classe que representa um cartão pré-pago associado a uma conta temporária. Ela estende a classe `Card`.

5. `TimeAccount` (Conta Temporária): Classe que representa uma conta temporária associada ao cartão pré-pago.

## Funcionalidades

1. **Conta Corrente (CurrentAccount)**: Permite sacar um valor menor ou igual ao saldo da conta corrente.

2. **Cartão de Crédito (CreditCard)**: Permite sacar um valor menor ou igual ao saldo da conta associada ao cartão de crédito.

3. **Cartão de Débito (DebitCard)**: Permite sacar um valor menor ou igual ao saldo da conta corrente associada ao cartão de débito.

4. **Cartão Pré-pago (PrepaidCard)**: Permite sacar um valor menor ou igual ao saldo da conta temporária associada ao cartão pré-pago. Quando o saldo da conta temporária atinge 0, ela é automaticamente desativada.

## Exemplo de Uso

```java
public class Main {
    public static void main(String[] args) {
        // Criando uma conta corrente com saldo de 500
        Account c1 = new CurrentAccount(500);
        
        // Criando um cartão de crédito associado à conta c1
        Card t1 = new CreditCard();
        t1.setAccount(c1);
        
        // Sacando 50 usando o cartão de crédito t1
        t1.withdraw(50); // c1.saldo = 450

        // Criando outra conta corrente com saldo de 100
        Account c2 = new CurrentAccount(100);
        
        // Criando um cartão de crédito associado à conta c2 com limite de 500
        Card t2 = new CreditCard();
        t2.setAccount(c2);
        
        // Sacando 50 usando o cartão de crédito t2
        t2.withdraw(50); // t2.saldo = 50

        // Criando uma conta temporária com saldo de 100
        Account c3 = new TimeAccount(100);
        
        // Criando um cartão pré-pago associado à conta temporária c3
        Card t3 = new PrepaidCard();
        t3.setAccount(c3);
        
        // Sacando 100 usando o cartão pré-pago t3
        t3.withdraw(100); // t3.saldo = 0, c3.desabilitada
    }
}
```
Output:

![executar](Assets/execucao.png)

***
<div class="profile-link">
  <a href="https://gitlab.com/hey_luannlucas" style="display: flex; align-items: center; text-decoration: none;">
    <img src="Assets/icons8-gitlab.svg" alt="GitLab" width="60" height="60" style="margin-right: 10px; border-radius: 50%;">
    <span class="name" style="font-size: 24px; font-weight: bold; color: #0000f;">Luann Lucas</span>
  </a>
</div>






