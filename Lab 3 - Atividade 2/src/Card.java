abstract class Card {
    protected Account account;

    public void setAccount(Account account) {
        this.account = account;
    }

    // Método abstrato para sacar dinheiro usando o cartão
    public abstract void withdraw(double amount);
}