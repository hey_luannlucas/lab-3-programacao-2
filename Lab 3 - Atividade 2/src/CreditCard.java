class CreditCard extends Card {
    // Implementação do método para sacar dinheiro usando o cartão de crédito
    public void withdraw(double amount) {
        if (account != null && amount <= account.getBalance()) {
            account.setBalance(account.getBalance() - amount);
            System.out.println("Saque de " + amount + " realizado com sucesso. Novo saldo: " + account.getBalance());
        } else {
            System.out.println("Saldo insuficiente para o saque.");
        }
    }
}
