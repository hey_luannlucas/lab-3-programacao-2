class CurrentAccount extends Account {
    public CurrentAccount(double initialBalance) {
        super(initialBalance);
    }

    // Implementação do método para sacar dinheiro da conta corrente
    public void withdraw(double amount) {
        if (amount <= balance) {
            balance -= amount;
        } else {
            System.out.println("Saldo insuficiente para o saque.");
        }
    }
}