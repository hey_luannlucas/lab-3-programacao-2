class DebitCard extends Card {
    // Implementação do método para sacar dinheiro usando o cartão de débito
    public void withdraw(double amount) {
        if (account != null && account instanceof CurrentAccount) {
            CurrentAccount currentAccount = (CurrentAccount) account;
            if (amount <= currentAccount.getBalance()) {
                currentAccount.setBalance(currentAccount.getBalance() - amount);
                System.out.println("Saque de " + amount + " realizado com sucesso. Novo saldo: " + currentAccount.getBalance());
            } else {
                System.out.println("Saldo insuficiente para o saque.");
            }
        } else {
            System.out.println("Cartão de débito associado apenas a conta corrente.");
        }
    }
}