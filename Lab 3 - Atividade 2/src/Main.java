public class Main {
    public static void main(String[] args) {
        // Criando uma conta corrente com saldo de 500
        Account c1 = new CurrentAccount(500);

        // Criando um cartão de crédito associado à conta c1
        Card t1 = new CreditCard();
        t1.setAccount(c1);

        // Sacando 50 usando o cartão de crédito t1
        t1.withdraw(50); // c1.saldo = 450

        // Criando outra conta corrente com saldo de 100
        Account c2 = new CurrentAccount(100);

        // Criando um cartão de crédito associado à conta c2 com limite de 500
        Card t2 = new CreditCard();
        t2.setAccount(c2);

        // Sacando 50 usando o cartão de crédito t2
        t2.withdraw(50); // t2.saldo = 50

        // Criando uma conta temporária com saldo de 100
        Account c3 = new TimeAccount(100);

        // Criando um cartão pré-pago associado à conta temporária c3
        Card t3 = new PrepaidCard();
        t3.setAccount(c3);

        // Sacando 100 usando o cartão pré-pago t3
        t3.withdraw(100); // t3.saldo = 0, c3.desabilitada
    }
}