class PrepaidCard extends Card {
    // Implementação do método para sacar dinheiro usando o cartão pré-pago
    public void withdraw(double amount) {
        if (account != null && account instanceof TimeAccount) {
            TimeAccount timeAccount = (TimeAccount) account;
            if (!timeAccount.isDisabled() && amount <= timeAccount.getBalance()) {
                timeAccount.setBalance(timeAccount.getBalance() - amount);
                if (timeAccount.getBalance() == 0) {
                    timeAccount.disableAccount();
                }
                System.out.println("Saque de " + amount + " realizado com sucesso. Novo saldo: " + timeAccount.getBalance());
            } else {
                System.out.println("Conta desativada ou saldo insuficiente para o saque.");
            }
        } else {
            System.out.println("Cartão pré-pago associado apenas a conta temporária.");
        }
    }
}