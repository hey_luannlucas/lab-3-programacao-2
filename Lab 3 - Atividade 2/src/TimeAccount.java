class TimeAccount extends Account {
    private boolean disabled;

    public TimeAccount(double initialBalance) {
        super(initialBalance);
        this.disabled = false;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void disableAccount() {
        disabled = true;
        System.out.println("Conta temporária desativada.");
    }

    // Implementação do método para sacar dinheiro da conta temporária
    public void withdraw(double amount) {
        if (!disabled && amount <= balance) {
            balance -= amount;
        } else {
            System.out.println("Conta desativada ou saldo insuficiente para o saque.");
        }
    }
}