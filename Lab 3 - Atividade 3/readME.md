## Disciplinas de Ensino 

Aqui temos classes e interfaces para gerenciar diferentes disciplinas e papéis de ensino. O código fornece uma estrutura flexível para atribuir disciplinas aos professores 
e demonstrar suas atividades de ensino.

### Diagrama de classes:
![](Assets/diagrama%20de%20classes.png)
### Estrutura do Código

1. **Interfaces**
   <br></br>

    - `Disciplina`: Esta interface declara um único método `getNome()` que retorna o nome de uma disciplina.
    - `Ensino`: Esta interface declara um único método `teach()` para ensinar uma disciplina específica.
<br></br>

2. **Classes**
   <br></br>

    - `Ciencias`, `Ginastica`, `Matematica` e `Musica`: Essas classes implementam a interface `Disciplina` e fornecem a implementação do método `getNome()` 
   para retornar os nomes de diferentes disciplinas.
      <br></br>
   `Substituto`: Esta classe implementa a interface `Ensino` e possui um campo privado `disciplina` do tipo `Disciplina`. 
   Ela fornece um método `assign()` para definir a disciplina atribuída e `teach()` para imprimir "Ditando" seguido do nome da disciplina atribuída.
      <br></br>
    - `Titular`: Esta classe também implementa a interface
      <br></br>
    - `Ensino` e possui um campo privado `disciplina` do tipo `Disciplina`. 
   Ela possui um construtor que recebe um objeto `Disciplina` e atribui ao campo `disciplina`. Além disso, ela fornece um método `teach()` para imprimir "Ensino de" seguido do nome da disciplina.
      <br></br>

3. **Classe Principal (Main)**

   A classe `Main` contém o método `main()` para demonstrar a funcionalidade das outras classes. 
Ela mostra como os professores (tanto titulares quanto substitutos) podem ser atribuídos a disciplinas e realizar suas atividades de ensino.


## Execução
```java
public class Main {
    public static void main(String[] args) {
        Titular titular = new Titular(new Ciencias());
        titular.teach(); // Saída: "Ensino de ciências"

        Substituto substituto = new Substituto();
        substituto.assign(new Ciencias());
        substituto.teach(); // Saída: "Ditando ciências"

        substituto.assign(new Ginastica());
        substituto.teach(); // Saída: "Ditando ginástica"

        substituto.assign(new Musica());
        substituto.teach(); // Saída: "Ditando música"
    }
}
```
**Output**:

![executar](Assets/execucao.png)
***
<div class="profile-link">
  <a href="https://gitlab.com/hey_luannlucas" style="display: flex; align-items: center; text-decoration: none;">
    <img src="Assets/icons8-gitlab.svg" alt="GitLab" width="60" height="60" style="margin-right: 10px; border-radius: 50%;">
    <span class="name" style="font-size: 24px; font-weight: bold; color: #0000f;">Luann Lucas</span>
  </a>
</div>



