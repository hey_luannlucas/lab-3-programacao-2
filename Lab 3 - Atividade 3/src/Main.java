public class Main {
    public static void main(String[] args) {
        Titular titular = new Titular(new Ciencias());
        titular.teach(); // Saída: "Ensino de ciências"

        Substituto substituto = new Substituto();
        substituto.assign(new Ciencias());
        substituto.teach(); // Saída: "Ditando ciências"

        substituto.assign(new Ginastica());
        substituto.teach(); // Saída: "Ditando ginástica"

        substituto.assign(new Musica());
        substituto.teach(); // Saída: "Ditando música"
    }
}
