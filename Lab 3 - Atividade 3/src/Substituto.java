class Substituto implements Ensino {
    private Disciplina disciplina;

    public void assign(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    @Override
    public void teach() {
        System.out.println("Ditando " + disciplina.getNome());
    }
}
