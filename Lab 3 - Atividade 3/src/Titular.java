class Titular implements Ensino {
    private Disciplina disciplina;

    public Titular(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    @Override
    public void teach() {
        System.out.println("Ensino de " + disciplina.getNome());
    }
}
